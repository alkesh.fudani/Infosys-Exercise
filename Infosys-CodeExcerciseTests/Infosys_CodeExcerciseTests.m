//
//  Infosys_CodeExcerciseTests.m
//  Infosys-CodeExcerciseTests
//
//  Created by Alkesh on 01/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 Test class that carries out perfomance as well as evaluation test based on the needs
*/

#import <XCTest/XCTest.h>
#import "ViewController.h"
#import "ControllerView.h"
#import "RequestManagerModel.h"
#import "InfoModel.h"

@interface Infosys_CodeExcerciseTests : XCTestCase
@property (nonatomic) ViewController *vcToTest;
@property (nonatomic) NSString *stringUrl;
@property(nonatomic, strong)ControllerView *viewObj;
@property(nonatomic, strong)RequestManagerModel *modelObj;

@end



@implementation Infosys_CodeExcerciseTests

- (void)setUp {
    [super setUp];
    self.stringUrl = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facets.json"];
    
    //Initialize view controller to test
    self.vcToTest = [[ViewController alloc]init];
    
    //setup model
    _modelObj = [[RequestManagerModel alloc] init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

// Unit test to check the perfomance of http web call
- (void)testPerformanceOfWebcall {
    // This is an example of a performance test case.
    [self measureBlock:^{
        [self.vcToTest fetchList];
        // Put the code you want to measure the time of here.
    }];
}

//Method to check if is there Null in any instance of the array
-(BOOL)checkIfThereAreNullRecords:(NSArray *)arr {
    if (arr) {
        for (InfoModel *obj in arr) {
            if (obj.title == nil || obj.title == (id)[NSNull null] || [obj.title isEqualToString:@""] || obj.descriptions == nil || obj.descriptions == (id)[NSNull null] || [obj.descriptions isEqualToString:@""] || obj.imageHref == nil || obj.imageHref == (id)[NSNull null] || [obj.imageHref isEqualToString:@""]) {
                return TRUE;
            }
        }
    }
    return FALSE;
    
}

//Unit test to check if there are records or not
-(void)testCheckRecordExists {
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Query timed out."];
    
    [_modelObj webCall:self.stringUrl success:^(NSString *title, NSMutableArray *ary) {
        XCTAssert(ary.count > 0, @"Records are available");
        [expectation fulfill];
    } failure:^(id responseData) {
        XCTAssert(@"No Records");
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        }
    }];
}

//Unit test to check if any object in the records contains null value
-(void)testCheckRecordContainsNull {
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Query timed out."];
    [_modelObj webCall:self.stringUrl success:^(NSString *title, NSMutableArray *ary) {
        BOOL isReceived = [self checkIfThereAreNullRecords:ary];
        XCTAssertEqual(isReceived,YES,@"There is a Null Value in one of the records");
        [expectation fulfill];
    } failure:^(id responseData) {
        XCTAssert(@"No Records");
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        }
    }];
}

@end
