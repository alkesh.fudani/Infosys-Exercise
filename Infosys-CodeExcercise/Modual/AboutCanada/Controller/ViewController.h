//
//  ViewController.h
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 31/08/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 View controller class that is tha bridge between the model and the view
*/

#import <UIKit/UIKit.h>
#import "RequestManagerModel.h"
#import "ControllerView.h"

@interface ViewController : UIViewController{
}

@property(nonatomic, strong)ControllerView *viewObj;
@property(nonatomic, strong)RequestManagerModel *modelObj;

- (void)fetchList;

@end

