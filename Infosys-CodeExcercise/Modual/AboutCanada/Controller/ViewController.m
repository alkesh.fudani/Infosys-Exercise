//
//  ViewController.m
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 31/08/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

#import "ViewController.h"
#import "Masonry.h"
@interface ViewController ()

@end

@implementation ViewController
@synthesize viewObj,modelObj;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    viewObj = [[ControllerView alloc] init];
    [viewObj setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:viewObj];
    
    [self setupConstraint];
    //add pull to refresh
    
    [viewObj.refreshControl addTarget:self
                            action:@selector(fetchList)
                  forControlEvents:UIControlEventValueChanged];
    //setup model
    modelObj = [[RequestManagerModel alloc] init];

    //call webservice.
    [self fetchList];
}


-(void)setupConstraint{
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [viewObj mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_topLayoutGuide).with.offset(padding.top);
        make.left.equalTo(self.view.mas_left).with.offset(padding.left);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-padding.bottom);
        make.right.equalTo(self.view.mas_right).with.offset(-padding.right);
    }];
    
}

- (void)fetchList{
    
    NSString *stringUrl = [NSString stringWithFormat:SERVER_URL];
    [modelObj webCall:stringUrl success:^(NSString *title, NSMutableArray *ary) {
        self.navigationItem.title = title;
        viewObj.arrayRecords = ary;
        [viewObj.tblView reloadData];
        [viewObj.refreshControl endRefreshing];
        [viewObj.tblView performSelector:@selector(reloadData) withObject:nil afterDelay:0.5];
        
    } failure:^(id responseData) {
        NSLog(@"%@",responseData);
    }];
    
}

@end
