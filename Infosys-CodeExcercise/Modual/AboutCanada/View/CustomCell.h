//
//  CustomCell.h
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 01/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 Custom cell class to create and load the TableViewCell programmatically
*/

#import <UIKit/UIKit.h>
#import "InfoModel.h"

@interface CustomCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *imgImageview;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblDescription;

-(void)loadData:(InfoModel *)shareobj;

@end
