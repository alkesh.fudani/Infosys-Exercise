//
//  ControllerView.h
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 03/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 The view class that gets added to the controller and hold base in the view
*/

#import <UIKit/UIKit.h>

@interface ControllerView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) UITableView *tblView;
@property (retain, nonatomic) UIRefreshControl *refreshControl;
@property (retain, nonatomic) NSMutableArray *arrayRecords;

-(void)setConstraint;

@end
