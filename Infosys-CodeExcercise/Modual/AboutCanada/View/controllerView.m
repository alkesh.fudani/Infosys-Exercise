//
//  ControllerView.m
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 03/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

#import "ControllerView.h"
#import "CustomCell.h"
#import "Masonry.h"
@implementation ControllerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)aRect
{
    if ((self = [super initWithFrame:aRect])) {
        [self setupTableviewandRefreshControll];

    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupTableviewandRefreshControll];
    }
    return self;
}

//Adding refresh control to tableview
-(void)setupTableviewandRefreshControll{
    
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.clipsToBounds = true;
    
    _tblView = [[UITableView alloc] init];
    [_tblView setTranslatesAutoresizingMaskIntoConstraints:NO];
    _tblView.dataSource = self;
    _tblView.delegate = self;
    _tblView.tableFooterView = [[UIView alloc] init];
    [self addSubview:_tblView];
    [self setConstraint];

    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor grayColor];

    [self.tblView addSubview:self.refreshControl];

}

//Setting constraints for tableview
-(void)setConstraint{
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tblView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
}


#pragma mark - tableview data source
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  70;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
    //    return 70.0f;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _arrayRecords.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomCell *cell = [[CustomCell alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [cell loadData:_arrayRecords[indexPath.row]];
    [cell layoutIfNeeded];
    return  cell;
}



@end
