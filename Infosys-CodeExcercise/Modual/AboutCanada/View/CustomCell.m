//
//  CustomCell.m
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 01/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

#import "CustomCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Masonry.h"
@implementation CustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //setup layout
        [self setupTitleLabel];
    }
    return self;
}

// method to setup imageview constraints
-(void)setupImageview{
    
    _imgImageview = [[UIImageView alloc] init];
    [_imgImageview setTranslatesAutoresizingMaskIntoConstraints:NO];
    _imgImageview.layer.cornerRadius  = 5;
    _imgImageview.layer.masksToBounds = true;
    [self.contentView addSubview:_imgImageview];
    
    [_imgImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(5.0);
        make.top.equalTo(_lblDescription.mas_bottom).with.offset(5.0);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-5.0);
        make.width.equalTo(_imgImageview.mas_height).with.offset(0);
    }];
    
}

// Method to setup title label constraints
-(void)setupTitleLabel{
    
    _lblTitle = [[UILabel alloc] init];
    [_lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblTitle.numberOfLines = 0;
    [self.contentView addSubview:_lblTitle];
    
    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(5.0);
        make.top.equalTo(self.contentView.mas_top).with.offset(5.0);
        make.trailing.equalTo(self.contentView.mas_trailing).with.offset(-5.0);
        make.height.equalTo(_lblTitle).with.offset(21.0);
    }];
    
    [self setupDescriptionLabel];
}

// Method to setup description label constraint
-(void)setupDescriptionLabel{
    _lblDescription = [[UILabel alloc] init];
    [_lblDescription setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblDescription.numberOfLines = 0;
    _lblDescription.lineBreakMode = NSLineBreakByWordWrapping;
    
    [self.contentView addSubview:_lblDescription];
    
    [_lblDescription mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(5.0);
        make.top.equalTo(_lblTitle.mas_bottom).with.offset(5.0);
        make.trailing.equalTo(self.contentView.mas_trailing).with.offset(-5.0);
        make.height.equalTo(_lblDescription).with.offset(0);
    }];

    [self setupImageview];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// loading the values into the cell
-(void)loadData:(InfoModel *)shareobj{
    _lblTitle.text = shareobj.title;
    _lblDescription.text = shareobj.descriptions;
    
    NSString *imgurl = shareobj.imageHref;
    __block UIImageView *imgview = _imgImageview;
    NSURLRequest *requst = [NSURLRequest requestWithURL:[NSURL URLWithString:imgurl]];
    [_imgImageview setImageWithURLRequest:requst placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        imgview.image = image;
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
    }];

}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

@end
