//
//  RequestManagerModel.h
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 03/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 A model class to take care of the web call and based on the success/failure responds to the controller
*/

#import <Foundation/Foundation.h>
typedef void(^WebMasterSuccessBlock)(NSString *title,NSMutableArray *ary);
typedef void(^WebMasterFailureBlock)(id responseData);

@interface RequestManagerModel : NSObject

-(void)webCall:(NSString *) stringUrl
       success:(WebMasterSuccessBlock)successBlock
       failure:(WebMasterFailureBlock)failureBlock;

@end




