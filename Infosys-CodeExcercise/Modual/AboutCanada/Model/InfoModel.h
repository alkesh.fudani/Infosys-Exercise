//
//  InfoModel.h
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 03/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 Attribute model class to store the data into the attributes
*/

#import <Foundation/Foundation.h>

@interface InfoModel : NSObject

@property (retain, nonatomic) NSString *title;
@property (retain, nonatomic) NSString *descriptions;
@property (retain, nonatomic) NSString *imageHref;

@end
