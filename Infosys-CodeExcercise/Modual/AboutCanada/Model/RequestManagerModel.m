//
//  RequestManagerModel.m
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 03/09/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

#import "RequestManagerModel.h"
#import <AFNetworking/AFNetworking.h>
#import "InfoModel.h"

@implementation RequestManagerModel


-(void)webCall:(NSString *) stringUrl
              success:(WebMasterSuccessBlock)successBlock
              failure:(WebMasterFailureBlock)failureBlock{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", nil];
    
    [manager GET:stringUrl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        
        NSString *title = responseObject[@"title"];
        if(title == nil && title == (id)[NSNull null]){
            title = @"";
        }
        
        NSMutableArray *arrayRecords = [[NSMutableArray alloc] init];
       for (NSDictionary *dict in responseObject[@"rows"]) {
           InfoModel *shareobj = [[InfoModel alloc] init];

           //check null
           NSString *title = [dict valueForKey:@"title"];
           if(title != nil && title != (id)[NSNull null]){
               shareobj.title = title;
           }else{
               shareobj.title = @"";
           }
           
           //check null
           NSString *descriptions = [dict valueForKey:@"description"];
           if(descriptions != nil && descriptions != (id)[NSNull null]){
               shareobj.descriptions = descriptions;
           }else{
               shareobj.descriptions = @"";
           }

           //check null
           NSString *imgurl = [dict valueForKey:@"imageHref"];
           if(imgurl != nil && imgurl != (id)[NSNull null]){
               shareobj.imageHref = imgurl;
           }else{
               shareobj.imageHref = @"";
           }
           
           //if all null then not insert into array.
           if([shareobj.title  isEqual: @""] && [shareobj.descriptions  isEqual: @""] && [shareobj.imageHref  isEqual: @""]){
           }else{
               [arrayRecords addObject:shareobj];
           }
       }
        
        successBlock(title,arrayRecords);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failureBlock(error);
    }];
    
}

@end
