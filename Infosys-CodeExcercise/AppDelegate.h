//
//  AppDelegate.h
//  Infosys-CodeExcercise
//
//  Created by Alkesh on 31/08/17.
//  Copyright © 2017 Alkesh. All rights reserved.
//

/**
 Default Appdelegate class and responsible for setting up the rootviewcontroller and adding a navigation controller to it.
*/

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

